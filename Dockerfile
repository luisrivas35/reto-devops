# Imagen usada como base 
FROM node:latest

# Workspace para la aplicacion
WORKDIR /reto-devops

# Package .json's de la aplicacion

COPY package*.json ./

# Instalar dependencias
RUN npm install

#Copiar el resto de la aplicacion
COPY . .

# Se ejecuta test
RUN npm run test

# Se expone el puerto 3000
EXPOSE 3000

# Usuario node para ejecutar la aplicacion
USER node

# Correr la aplicacion
CMD [ "node", "index.js" ]
